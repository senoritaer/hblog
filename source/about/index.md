---
title: 关于
date: 2015-09-20 23:00:53
---

## 关于本博客 ##

这个博客是基于hexo+github+travis-ci+coding-pages搭建。
将源码push到github，通过travis-ci自动构建后推送并部署到coding-pages上。

主要记录我生活和学习中的遇到的一些困难以及解决办法，作为自己经验和知识的积累，留给自己以后做参考，踩过的`凹`不要再踩就好。

## 联系方式 ##

```
EMAIL: bWVAZ2VudGxlaHUuY29tCg== (base64 encode,just for avoid SPAM)
```

you can decode and see my email by run following command at your computer terminal:
```bash
echo bWVAZ2VudGxlaHUuY29tCg== | base64 -d
```

- if you have some question, please feel free to ask me via email.
- if you find any inaccurate information about my blog please send an email to tell me and I will amend it as soon as possible.


<!--![wallpaper](/images/2017/09/my-wallpaper.jpg)-->
