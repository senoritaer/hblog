# hexo-blog

![Build Status](https://travis-ci.org/Akame-moe/hexo-blog.svg?branch=master)  ![Hexo](https://img.shields.io/badge/hexo-3.3.8-blue.svg)

### 这是hexo-blog的源码

使用travis-ci自动化生成和部署到coding.net的pages服务上。也许你会问为什么不使用github的pages服务，有一下两点：

1. github的pages服务器在国外，在国内访问会比较慢  
2. coding的pages服务支持强制使用https访问。  


